const getSum = (str1, str2) => {
  if (
    typeof str1 !== 'string' ||
    typeof str2 !== 'string' ||
    (+str1).toString() === 'NaN' ||
    (+str2).toString() === 'NaN'
  ) {
    return false;
  }
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countPosts = 0;
  let countComents = 0;
  for (let post of listOfPosts) {
    if (post.author === authorName) countPosts++;

    if (post.comments) {
      for (let coment of post.comments) {
        if (coment.author === authorName) countComents++;
      }
    }
  }
  return `Post:${countPosts},comments:${countComents}`;
};

const tickets = (people) => {
  let cash = 0;
  for (let i of people) {
    let rest = +i - 25;
    cash += 25 - rest;
    if (cash < 0) return 'NO';
  }
  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
